import React, { Component } from "react";

import "./glassItem.css";

class GlassItem extends Component {
  render() {
    return (
      <div className="glass-item col-6 col-sm-4 col-md-2 col-xl">
        <button
          className="d-flex align-items-center justify-content-center h-50 overflow-hidden p-2 bg-transparent"
          onClick={() => this.props.setSelectedGlass(this.props.item)}
        >
          <img
            className="d-block w-100"
            src={this.props.item.url_jpg}
            alt="glass"
          />
        </button>
      </div>
    );
  }
}

export default GlassItem;
