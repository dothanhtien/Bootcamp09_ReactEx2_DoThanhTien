import React, { Component } from "react";

import GlassItem from "./glassItem";

class GlassList extends Component {
  renderGlassList = () => {
    return this.props.glassList.map((item) => {
      return (
        <GlassItem
          key={item.id}
          item={item}
          setSelectedGlass={this.props.setSelectedGlass}
        />
      );
    });
  };

  render() {
    return (
      <div className="glasses row bg-white mt-5 p-3">
        {this.renderGlassList()}
      </div>
    );
  }
}

export default GlassList;
