import React, { Component } from "react";

import "./model.css";

class Model extends Component {
  render() {
    return (
      <div className="model mx-auto">
        <img
          src="./glassesImage/model.jpg"
          className="model__image w-100"
          alt="model"
        />
        {this.props.selectedGlass && (
          <>
            <img
              src={this.props.selectedGlass.url_png}
              className="model__glass"
              alt="glass"
            />
            <div className="model__overlay p-2">
              <h6 className="font-weight-bold">
                {this.props.selectedGlass.name}
              </h6>
              <p className="mb-0">{this.props.selectedGlass.desc}</p>
            </div>
          </>
        )}
      </div>
    );
  }
}

export default Model;
