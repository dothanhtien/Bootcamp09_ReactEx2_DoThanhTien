import React, { Component } from "react";

import Model from "./model";
import GlassList from "./glassList";
import "./home.css";

class Home extends Component {
  state = {
    selectedGlass: null,
  };

  glasses = [
    {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url_png: "./glassesImage/v1.png",
      url_jpg: "./glassesImage/g1.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
    {
      id: 2,
      price: 50,
      name: "GUCCI G8759H",
      url_png: "./glassesImage/v2.png",
      url_jpg: "./glassesImage/g2.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
    {
      id: 3,
      price: 30,
      name: "DIOR D6700HQ",
      url_png: "./glassesImage/v3.png",
      url_jpg: "./glassesImage/g3.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
    {
      id: 4,
      price: 30,
      name: "DIOR D6005U",
      url_png: "./glassesImage/v4.png",
      url_jpg: "./glassesImage/g4.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
    {
      id: 5,
      price: 30,
      name: "PRADA P8750",
      url_png: "./glassesImage/v5.png",
      url_jpg: "./glassesImage/g5.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
    {
      id: 6,
      price: 30,
      name: "PRADA P9700",
      url_png: "./glassesImage/v6.png",
      url_jpg: "./glassesImage/g6.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
    {
      id: 7,
      price: 30,
      name: "FENDI F8750",
      url_png: "./glassesImage/v7.png",
      url_jpg: "./glassesImage/g7.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
    {
      id: 8,
      price: 30,
      name: "FENDI F8500",
      url_png: "./glassesImage/v8.png",
      url_jpg: "./glassesImage/g8.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
    {
      id: 9,
      price: 30,
      name: "FENDI F4300",
      url_png: "./glassesImage/v9.png",
      url_jpg: "./glassesImage/g9.jpg",
      desc: "Light pink square lenses define these sunglasses, ending withamother of pearl effect tip. ",
    },
  ];

  setSelectedGlass = (item) => {
    this.setState({
      selectedGlass: item,
    });
  };

  render() {
    return (
      <div
        className="home"
        style={{ backgroundImage: "url(./glassesImage/background.jpg)" }}
      >
        <h1 className="text-center text-white py-4 mb-0">
          TRY GLASSES APP ONLINE
        </h1>
        <div className="container py-5">
          <Model selectedGlass={this.state.selectedGlass} />
          <GlassList
            glassList={this.glasses}
            setSelectedGlass={this.setSelectedGlass}
          />
        </div>
      </div>
    );
  }
}

export default Home;
