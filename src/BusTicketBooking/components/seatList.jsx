import React, { Component } from "react";

import SeatItem from "./seatItem";
import "./seatList.css";

class SeatList extends Component {
  renderSeatList = () => {
    return this.props.seatList.map((item) => {
      return (
        <SeatItem
          key={item.SoGhe}
          item={item}
          bookingSeatList={this.props.bookingSeatList}
          setBookingSeat={this.props.setBookingSeat}
        />
      );
    });
  };

  render() {
    return (
      <div className="seat-list col-md-6">
        <span className="d-block text-center py-2">Tài xế</span>
        <div className="d-flex flex-wrap">{this.renderSeatList()}</div>
      </div>
    );
  }
}

export default SeatList;
