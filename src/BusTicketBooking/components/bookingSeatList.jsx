import React, { Component } from "react";

import BookingSeatItem from "./bookingSeatItem";
import "./bookingSeatList.css";

class BookingSeatList extends Component {
  renderBookingSeatList = () => {
    return this.props.bookingSeatList.map((item) => {
      return (
        <BookingSeatItem
          key={item.SoGhe}
          item={item}
          setBookingSeat={this.props.setBookingSeat}
        />
      );
    });
  };

  render() {
    return (
      <div className="booking-list col-md-6">
        <h4>Danh sách ghế đã đặt ({this.props.bookingSeatList.length})</h4>
        {this.renderBookingSeatList()}
      </div>
    );
  }
}

export default BookingSeatList;
