import React, { Component } from "react";

class BookingSeatItem extends Component {
  render() {
    const { SoGhe, Gia } = this.props.item;

    return (
      <div className="d-flex align-items-center">
        <span>
          Ghế: số {SoGhe} ${Gia}
        </span>
        <button
          className="btn btn-link text-danger text-decoration-none"
          onClick={() => this.props.setBookingSeat(this.props.item)}
        >
          [Hủy]
        </button>
      </div>
    );
  }
}

export default BookingSeatItem;
