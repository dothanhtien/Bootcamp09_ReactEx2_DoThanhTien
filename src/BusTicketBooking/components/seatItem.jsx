import React, { Component } from "react";

import "./seatItem.css";

class SeatItem extends Component {
  checkIsBooking = () => {
    const foundItem = this.props.bookingSeatList.find(
      (item) => item.SoGhe === this.props.item.SoGhe
    );

    return foundItem === undefined ? false : true;
  };

  render() {
    const { SoGhe, TrangThai } = this.props.item;

    const btnClasses = `w-100 btn ${
      this.checkIsBooking() ? "btn-success" : "btn-secondary"
    }`;

    return (
      <div className="seat-item col-3">
        <div className="seat-item__container w-100">
          <button
            className={btnClasses}
            disabled={TrangThai}
            onClick={() => this.props.setBookingSeat(this.props.item)}
          >
            {SoGhe}
          </button>
        </div>
      </div>
    );
  }
}

export default SeatItem;
